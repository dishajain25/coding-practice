/*

	1
	2	4
	3	6	9
	4	8	12	16

*/

#include<iostream>
int main(){

	int row;
	std::cout<<"Enter No of Rows : ";
	std::cin>>row;

	for(int i=1; i<=row; i++){

		for(int j=1;j<=i; j++){

			std::cout<<i*j<<"\t";

		}
		std::cout<<std::endl;

	}
}


