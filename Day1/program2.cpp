/*

	17	15	13
	11	9	7
	5	3	1

*/

#include<iostream>
int main(){

	int row,col;
	std::cout<<"Enter No of Rows : ";
	std::cin>>row;

	std::cout<<"Enter No of Column : ";
	std::cin>>col;

	int num = 2*(row*row) - 1;

	for(int i=1; i<=row; i++){

		for(int j=1;j<=col; j++){

			std::cout<<num<<"\t";
			num = num-2;

		}
		std::cout<<std::endl;

	}
}


