/*

	a	
	b	a
	c	b	a
	d	c	b	a

*/

#include<iostream>
int main(){

	int row,col;
	std::cout<<"Enter No of Rows : ";
	std::cin>>row;

	for(int i=1; i<=row; i++){

		char ch = 96+i;

		for(int j=1;j<=i; j++){

			std::cout<<ch<<"\t";
			ch--;

		}
		std::cout<<std::endl;
	}
}


