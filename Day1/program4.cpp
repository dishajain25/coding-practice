/*
	5	4	3	2	1	
	D	C	B	A
	3	2	1
	B	A
	1

*/

#include<iostream>
int main(){

	int row,col;
	std::cout<<"Enter No of Rows : ";
	std::cin>>row;

	for(int i=1; i<=row; i++){

		int num = row-i+1;
		char ch = 'A' + row - i;

		for(int j=row;j>=i; j--){

			if(i%2 != 0){
				std::cout<<num<<"\t";
				num --;
			}else{
				std::cout<<ch<<"\t";
				ch--;

			}
		}

		std::cout<<std::endl;
	}
}


