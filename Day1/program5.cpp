/*
	4	4	4	4		
	C	C	C	
	2	2	
	A


*/

#include<iostream>
int main(){

	int row,col;
	std::cout<<"Enter No of Rows : ";
	std::cin>>row;

	int num = row;
	char ch = 'A' + row - 1;

	for(int i=1; i<=row; i++){

		for(int j=row;j>=i; j--){

			if(i%2 != 0){
				std::cout<<num<<"\t";
			}else{
				std::cout<<ch<<"\t";

			}
		}
		num--;
		ch--;
		std::cout<<std::endl;
	}
}


