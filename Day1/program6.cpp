/*

   C++ Program to Check Whether a Number is Prime or Not

*/

#include<iostream>

int main(){

	int num;
	std::cout<<"Enter the Number : ";
	std::cin>>num;

	bool isPrime = true;

	if(num == 0 || num == 1)
		isPrime = false;

	for(int i = 2; i <= num/2; i++){

		if(num % i == 0){

			isPrime = false;
			break;
		}
	}

	if(isPrime){

		std::cout<<num<<" is a prime number"<<std::endl;

	}else{

		std::cout<<num<<" is Not a prime number"<<std::endl;

	}
}
