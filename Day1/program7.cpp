/*

   C++ Program to Check Whether a Number is Prime or Not in given Range.

*/

#include <bits/stdc++.h>
int main()
{
	
	int a, b,flag;

	std::cout << "Enter lower bound : ";
	std::cin >> a;

	std::cout << "Enter upper bound : ";
	std::cin >> b;

	std::cout << "Prime numbers between " << a << " and " << b << " are: ";

	for (int i = a; i <= b; i++) {
		if (i == 1 || i == 0)
			continue;

		flag = 1;
		for (int j = 2; j <= i / 2;j++) {
			if (i % j == 0) {
				flag = 0;
				break;
			}
		}

		if (flag == 1)
			std::cout << i << " ";
	}
	std::cout<<std::endl;
	return 0;
}

